package org.osmmultithreaddownloader.util;
/**
 * ProxyController.java
 * 
 * Builds a map of proxys (with proxy and port) and gives a random proxy
 * from the map if is it required by other class.
 * 
 * Copyright (C) 2016 Victor Purcallas <vpurcallas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ProxyController {
	private  Map<String, String> proxyUrlAndPort;
	
	
	
	public ProxyController() {
		this.proxyUrlAndPort = new HashMap<String, String>();
		buildProxUrlAndPortMap();
	}
	
	public Proxy getRandomProxy(){
		
		List<Map.Entry<String, String>> listProxy = new ArrayList<Map.Entry<String, String>>(
				proxyUrlAndPort.entrySet());
		Collections.shuffle(listProxy);
		String proxyUrl = "";
		String proxyPort = "";
		for (Map.Entry<String, String> entry2 : listProxy) {
			proxyUrl = entry2.getKey();
			proxyPort = entry2.getValue();
			break;
		};
		
		Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyUrl, Integer.parseInt(proxyPort)));
		return proxy;
	}

	// Free proxies list
			private void buildProxUrlAndPortMap() {
				putProxyUrlAndPort("85.214.221.46", "1080");
				putProxyUrlAndPort("213.136.79.124", "80");
				putProxyUrlAndPort("213.136.79.122", "80");
				putProxyUrlAndPort("144.76.232.58", "3128");
				putProxyUrlAndPort("79.143.188.122", "3128");
				putProxyUrlAndPort("188.40.62.138", "80");
				putProxyUrlAndPort("46.128.54.115", "10200");
				putProxyUrlAndPort("5.189.144.20", "3128");
				putProxyUrlAndPort("46.165.247.148", "80");
				putProxyUrlAndPort("89.163.131.168", "3128");
				putProxyUrlAndPort("88.198.248.19", "8080");
				putProxyUrlAndPort("188.195.104.119", "80");
				putProxyUrlAndPort("85.216.41.254", "80");
				putProxyUrlAndPort("212.78.104.252", "80");
				putProxyUrlAndPort("212.78.103.84", "80");
				putProxyUrlAndPort("217.92.181.91", "8080");
				putProxyUrlAndPort("155.133.7.47 ", "8080");
				putProxyUrlAndPort("46.4.255.121", "8080");
				putProxyUrlAndPort("37.186.214.39", "80");
				putProxyUrlAndPort("195.88.7.152", "80");
				putProxyUrlAndPort("83.86.180.211", "80");
				putProxyUrlAndPort("89.98.24.196", "80");
				putProxyUrlAndPort("92.254.20.67", "80");
				putProxyUrlAndPort("37.188.72.122", "80");
				putProxyUrlAndPort("85.221.54.4", "80");
				putProxyUrlAndPort("46.14.148.45", "80");
				putProxyUrlAndPort("213.144.132.109", "80");
				putProxyUrlAndPort("80.219.224.247", "80");
				putProxyUrlAndPort("77.58.86.128", "80");
				putProxyUrlAndPort("213.60.151.29", "80");
				putProxyUrlAndPort("194.140.140.133", "80");
				putProxyUrlAndPort("195.76.101.82", "8080");
				putProxyUrlAndPort("146.120.111.42", "8080");
				putProxyUrlAndPort("185.28.193.95", "8080");
				putProxyUrlAndPort("80.188.135.10", "8080");
				putProxyUrlAndPort("93.99.212.44", "8080");
				putProxyUrlAndPort("176.96.154.154", "11002");
				putProxyUrlAndPort("194.78.119.91", "8080");
				putProxyUrlAndPort("85.114.54.81", "80");
				putProxyUrlAndPort("85.10.53.90", "8080");
				putProxyUrlAndPort("83.208.101.54", "1080");
				putProxyUrlAndPort("193.179.14.28", "1080");
				putProxyUrlAndPort("185.28.193.95", "8080");
				putProxyUrlAndPort("146.120.111.42", "8080");
				putProxyUrlAndPort("88.83.233.160", "1080");
				putProxyUrlAndPort("95.168.217.24", "3128");
				putProxyUrlAndPort("80.188.135.10", "8080");
				putProxyUrlAndPort("93.99.212.44", "8080");
				putProxyUrlAndPort("94.229.89.87", "1080");
				putProxyUrlAndPort("83.91.86.26", "9100");
				putProxyUrlAndPort("85.23.223.32", "8080");
				putProxyUrlAndPort("194.136.142.211", "8080");
				putProxyUrlAndPort("194.136.143.18", "8080");
				putProxyUrlAndPort("37.187.60.61", "37148");
				putProxyUrlAndPort("178.33.227.212", "3128");
				putProxyUrlAndPort("195.154.231.43", "3128");
				putProxyUrlAndPort("149.202.68.167", "37135");
				putProxyUrlAndPort("178.22.148.122", "3129");
				putProxyUrlAndPort("37.187.125.39", "8080");
				putProxyUrlAndPort("149.202.40.200", "3128");
				putProxyUrlAndPort("94.23.200.49", "3128");
				putProxyUrlAndPort("5.196.89.170", "80");
				putProxyUrlAndPort("51.254.103.206", "3128");
				//putProxyUrlAndPort("87.98.221.137", "4050");
				putProxyUrlAndPort("87.98.218.86", "3128");
				putProxyUrlAndPort("78.226.250.120", "8080");
				putProxyUrlAndPort("51.254.30.26", "80");
				putProxyUrlAndPort("51.255.165.213", "8080");
				putProxyUrlAndPort("37.187.253.39", "8184");
				putProxyUrlAndPort("51.255.196.204", "3128");
				putProxyUrlAndPort("80.14.12.161", "80");
				putProxyUrlAndPort("51.255.161.222", "80");
				putProxyUrlAndPort("176.31.239.33", "8123");
				putProxyUrlAndPort("80.13.39.143", "3128");
				putProxyUrlAndPort("178.33.191.53", "3128");
				putProxyUrlAndPort("51.254.32.143", "80");
				putProxyUrlAndPort("178.22.151.103", "80");
				putProxyUrlAndPort("46.218.85.101", "3129");
				putProxyUrlAndPort("92.222.237.7", "8888");
				putProxyUrlAndPort("178.32.87.230", "3128");
				putProxyUrlAndPort("92.222.237.37", "8888");
				putProxyUrlAndPort("92.222.237.5", "8888");
				putProxyUrlAndPort("46.105.152.96", "8898");
				putProxyUrlAndPort("92.222.237.60", "8898");
				putProxyUrlAndPort("83.170.105.101", "8081");
				putProxyUrlAndPort("151.236.56.190", "3128");
				putProxyUrlAndPort("128.199.147.23", "8080");
				putProxyUrlAndPort("37.130.234.15", "80");
				putProxyUrlAndPort("212.48.85.164", "80");
				putProxyUrlAndPort("151.236.63.217", "3128");
				putProxyUrlAndPort("89.206.236.236", "80");
				putProxyUrlAndPort("89.206.186.144", "80");
				putProxyUrlAndPort("128.199.115.31", "8080");
				putProxyUrlAndPort("213.165.155.189", "80");
				putProxyUrlAndPort("128.199.184.159", "8080");
				putProxyUrlAndPort("128.199.138.206", "3128");
				putProxyUrlAndPort("194.203.40.189", "8085");
				putProxyUrlAndPort("185.116.213.46", "3128");
				putProxyUrlAndPort("194.203.40.189", "8085");
				putProxyUrlAndPort("185.116.213.46", "3128");
				putProxyUrlAndPort("62.23.15.92", "3128");
				putProxyUrlAndPort("81.199.154.115", "8080");
				putProxyUrlAndPort("128.199.232.210", "3128");
				putProxyUrlAndPort("176.227.202.26", "9574");
				putProxyUrlAndPort("89.200.142.236", "1080");
				putProxyUrlAndPort("212.17.44.26", "1080");
				putProxyUrlAndPort("82.195.156.16", "3128");
				putProxyUrlAndPort("77.67.17.201", "8080");
				putProxyUrlAndPort("89.101.214.214", "8080");
				putProxyUrlAndPort("185.63.101.87", "1080");
				putProxyUrlAndPort("83.211.159.89", "1080");
				putProxyUrlAndPort("31.199.192.20", "1080");
				putProxyUrlAndPort("151.1.216.181", "80");
				//putProxyUrlAndPort("150.145.1.87", "9050");
				putProxyUrlAndPort("2.228.146.39", "1080");
				putProxyUrlAndPort("37.186.214.39", "80");
				putProxyUrlAndPort("91.143.199.80", "3128");
				putProxyUrlAndPort("88.32.124.85", "80");
				putProxyUrlAndPort("151.80.240.50", "3128");
				putProxyUrlAndPort("80.19.159.242", "8080");
				putProxyUrlAndPort("185.58.225.191", "8080");
				putProxyUrlAndPort("151.80.197.192", "80");
				putProxyUrlAndPort("185.97.123.211", "8888");
				putProxyUrlAndPort("212.3.169.240", "1080");
				putProxyUrlAndPort("213.151.206.178", "1080");
				putProxyUrlAndPort("176.10.43.57", "1080");
				putProxyUrlAndPort("176.10.40.242", "1080");
				putProxyUrlAndPort("109.236.126.130", "8080");
				putProxyUrlAndPort("109.236.122.243", "8080");
				putProxyUrlAndPort("176.10.41.143", "1080");
				putProxyUrlAndPort("109.236.122.2", "8080");
				putProxyUrlAndPort("87.197.177.252", "3128");

			}

			private void putProxyUrlAndPort(String proxyUrl, String proxyPort) {
				proxyUrlAndPort.put(proxyUrl, proxyPort);
			}
}
