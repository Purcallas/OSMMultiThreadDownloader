package org.osmmultithreaddownloader.util;
/**
 * UIMessenger.java
 * 
 * Gives console information for user about the process performance
 * 
 * Copyright (C) 2016 Victor Purcallas <vpurcallas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public class UIMesseger {
	
	
	static final int SATISFACTORY_CHECKED = 701;
	static final int SATISFACTORY_DOWNLOADED = 702; 
	static final int GET_FILE_LENGHT_ERROR = 703; 
	static final int URL_EXCEPTION_ERROR = 704; 
	static final int IO_GET_LENGHT_ERROR = 705; 
	static final int DOWNLOADED_FILE_SIZE_ERROR = 706;
	static final int IO_DOWNLOAD_FILE_ERROR = 707;
	
	static int partialCounter = 0;
	
	static int totalSatisfactory = 0;
	static int satisfactoryChecked = 0; 		//701
	static int satisfactoryDownload = 0;		//702
	
	static int totalErrors = 0;
	static int getFileServerLenghtError = 0;	//703
	static int exceptionInUrlError = 0;			//704
	static int IOExceptionErrorInGetLenght = 0;	//705
	static int downloadedFileSizeError = 0;		//706
	static int IOExceptionErrorInDownload = 0;	//707
	
	static int totalProcessFiles = 0;
	static int futureTasks = 0;
	
	static int previousPercent = -1;

	
	public static void printStartProcess(int totalProcessFiles){
		UIMesseger.totalProcessFiles = totalProcessFiles;
		System.out.println("START PROCESS");
		System.out.println("==============");
	}
	
	public static void printProcessing(int futureTasks){
		clearCounters();
		UIMesseger.futureTasks = futureTasks;
		System.out.println("PROCESSING");
		System.out.println("==========");
		System.out.println("Files to process: " + futureTasks);
	}
	
	private static void clearCounters(){
		partialCounter = 0;
		totalSatisfactory = 0;
		satisfactoryChecked = 0; 		
		satisfactoryDownload = 0;		
		totalErrors = 0;
		getFileServerLenghtError = 0;	
		exceptionInUrlError = 0;			
		IOExceptionErrorInGetLenght = 0;	
		downloadedFileSizeError = 0;		
		IOExceptionErrorInDownload = 0;
		previousPercent = -1;
	}
	
	public static void addPercentage(int partialCounter){
		UIMesseger.partialCounter = partialCounter;
		String message = ""+(partialCounter * 100 /futureTasks)+ "%";
		if ((partialCounter * 100 /futureTasks)>UIMesseger.previousPercent){
			if ((partialCounter * 100 / futureTasks)%5==0){
				System.out.println(message);
				UIMesseger.previousPercent = (partialCounter * 100 /futureTasks);
				}
		}
		
	}
	
	public static void addCode(int code){

		switch(code){
		case SATISFACTORY_CHECKED:
			UIMesseger.satisfactoryChecked++;
			break;
		case SATISFACTORY_DOWNLOADED:
			UIMesseger.satisfactoryDownload++;
			break;
		case GET_FILE_LENGHT_ERROR:
			UIMesseger.getFileServerLenghtError++;
			break;
		case URL_EXCEPTION_ERROR:
			UIMesseger.exceptionInUrlError++;
			break;
		case IO_GET_LENGHT_ERROR:
			UIMesseger.IOExceptionErrorInGetLenght++;
			break;
		case DOWNLOADED_FILE_SIZE_ERROR:
			UIMesseger.downloadedFileSizeError++;
			break;
		case IO_DOWNLOAD_FILE_ERROR:
			UIMesseger.IOExceptionErrorInDownload++;
			break;
		}
	}
	
	public static void printResults(int futureTasks){
		UIMesseger.totalSatisfactory = UIMesseger.satisfactoryChecked+UIMesseger.satisfactoryDownload;
		UIMesseger.totalErrors = UIMesseger.getFileServerLenghtError + UIMesseger.exceptionInUrlError + UIMesseger.IOExceptionErrorInGetLenght + UIMesseger.downloadedFileSizeError + UIMesseger.IOExceptionErrorInDownload;
		
		System.out.println();
		System.out.println("Total process completed " +((totalProcessFiles -futureTasks)*100/totalProcessFiles) +"% |" + (totalProcessFiles - futureTasks) +"/"+ totalProcessFiles);
		System.out.println("==============================================");
		System.out.println("Satisfactory recived "+ UIMesseger.totalSatisfactory);
		System.out.println("Errors recived "+ UIMesseger.totalErrors);
		
		if (totalErrors == 0){
			System.out.println("=================================");
			System.out.println("ALL FILES ARE STORED SUCCESSFULLY");
			System.out.println("=================================");
		}else{
			System.out.println("Parsing errors");
			System.out.println("");
		}
	}
	

}
