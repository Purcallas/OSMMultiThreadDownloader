package org.osmmultithreaddownloader.util;
/**
 * DownloadThread.java
 * 
 * This class launches a download process and returns a code depending
 * the process performance.
 * 
 * It uses the class Proxy Controller to use a random proxy in its connections
 * 
 * Copyright (C) 2016 Victor Purcallas <vpurcallas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.util.concurrent.Callable;
 
public class DownloadThread implements Callable<DownloadThread>{
	private static final int BYTES_MARGIN = 350;
	private String fileUrl;
	private String filePath;
	private int finishedResult;
	
	
	public DownloadThread(String fileUrl,
			String filePath) {
		super();
		this.fileUrl = fileUrl;
		this.filePath = filePath;
	}
	
	

	@Override
	public DownloadThread call() {
		// TODO Auto-generated method stub
		int checkedStatus = 700;
		try {
			URL url = new URL(this.fileUrl);
			File f = new File(filePath);
			//Get random proxy
			ProxyController pc = new ProxyController();
			Proxy proxy = pc.getRandomProxy();
			HttpURLConnection conn = (HttpURLConnection) url.openConnection(proxy);
			conn.setConnectTimeout(10000);
			conn.setReadTimeout(15000);
			conn.setRequestMethod("GET");
			conn.getInputStream();
			int contentLegth = conn.getContentLength();
			
			if (f.exists() && !f.isDirectory()) {
				//System.out.println("FILE LENGTH " + f.length());
				int validePercent = BYTES_MARGIN;
				//File correct
				if (contentLegth <= f.length() + validePercent && contentLegth >= f.length() - validePercent) {
					checkedStatus = 701;
					//download(contentLegth)
					//filePath = null;
				} else {
					if (contentLegth == -1){
						//Bad length get value
						checkedStatus = 703;
					}else{
						checkedStatus = this.download(proxy,contentLegth);
					}
				}
			} else {
				checkedStatus = this.download(proxy,contentLegth);
			}
		} catch (MalformedURLException e) {
			//Exception in Url
			checkedStatus = 704;
			//e.printStackTrace();
		} catch (IOException e) {
			//Exception in get the lenght
			checkedStatus = 705;
			//e.printStackTrace();
		} 
		this.finishedResult = checkedStatus;
		return this;
	}
	
	private int download(Proxy proxy, long fileLength) {
		int checkedStatus = 704;
		// Next connection will be through proxy.
				URL url;
				try {
					url = new URL(this.fileUrl);
					HttpURLConnection conn = (HttpURLConnection) url.openConnection(proxy);
					conn.setConnectTimeout(10000);
					conn.setReadTimeout(25000);
					conn.setRequestMethod("GET");
					InputStream in = conn.getInputStream();
					
					
					//InputStream in = url.openStream();
					
					String directory = "";
					String[] directoryParts= filePath.split(File.separator);
					for ( int i = 1 ; i < (directoryParts.length - 1) ; i++ ){
						directory += directoryParts[i] + File.separator;
					}
					
					//Build the directory
					File fileDir=new File(directory);
			        if(!fileDir.exists()){
			            fileDir.mkdirs();
			        }
					
					//Save the file
					OutputStream out = new BufferedOutputStream(new FileOutputStream(filePath));
					for ( int i; (i = in.read()) != -1; ) {
					    out.write(i);
					}
					in.close();
					out.close();
					
					File f = new File(filePath);
					int validePercent = BYTES_MARGIN;
					if ((fileLength <= f.length() + validePercent && fileLength >= f.length() - validePercent)) {						
						checkedStatus = 702;
					}else{
						checkedStatus = 706;
					}
				} catch (IOException e) {
					checkedStatus = 707;
				}
				
				return checkedStatus;
	}

	public String getFileUrl() {
		return fileUrl;
	}



	public String getFilePath() {
		return filePath;
	}
	
	public int isFinishedResult() {
		return finishedResult;
	}
	
	






	



	
   

}
