package org.osmmultithreaddownloader.main;

/**
 * MultiThreadDownloader.java
 * 
 * This class manage the download from OpenStreetMaps using a defined number
 * of threads to do it. 
 * 
 * It builds the necessary URLs and file paths depending the given minimum depth 
 * and the max depth of the world map required by user.
 * 
 * It uses DownloadProxyThread to launch the threads through and Executor. And 
 * also calls UIMessenger to inform the user about process status.
 * 
 * Copyright (C) 2016 Victor Purcallas <vpurcallas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.osmmultithreaddownloader.util.DownloadThread;
import org.osmmultithreaddownloader.util.UIMesseger;

public class MultiThreadDownloader {
	private int minDepth = 0;
	private int maxDepth = 3;
	private int CONCURRENT_THREADS = 50;

	private final String ROOT_PATH = "." + File.separator + "map"
			+ File.separator;
	private final String FORMATTED_DESTINATION_URL = "http://b.tile.openstreetmap.org/%d/%d/%d.png";

	

	private Map<String, String> filePathAndUrlMap;
	private ExecutorService threadPool;

	public static void main(String[] args) {

		MultiThreadDownloader tc = new MultiThreadDownloader();
		boolean start = entranceArgs(args, tc);
		if (start) {
			tc.run();
		}
	}

	private static Boolean entranceArgs(String[] args, MultiThreadDownloader tc) {
		Boolean start = false;
			if (args.length == 2) {
				try {
					tc.minDepth = Integer.parseInt(args[0]);
					tc.maxDepth = Integer.parseInt(args[1]);
					start = true;
				} catch (Exception e) {
					showInstructions();
					start = false;
				}
		} else if(args.length==4){
			try {
				tc.minDepth = Integer.parseInt(args[0]);
				tc.maxDepth = Integer.parseInt(args[1]);
				if (args[2].equals("-t")){
					tc.CONCURRENT_THREADS = Integer.parseInt(args[3]);
					start = true;
				}else{
					showInstructions();
				}
			} catch (Exception e) {
				showInstructions();
			}
			
		} else {
			showInstructions();
		}
		return start;
	}

	private static void showInstructions() {
		System.out.println("Atention");
		System.out.println("========");
		System.out
				.println("osmmultithreaddownloader.jar [minDepth:number] [maxDepth:number] required | [-t] [numberOfThreads:number] optional ");
		System.out
				.println("Example: osmmultithreaddownloader.jar 0 4");
		System.out.println("Example: osmmultithreaddownloader.jar 0 4 -t 100");
	}

	public MultiThreadDownloader() {
		super();
		this.filePathAndUrlMap = new HashMap<String, String>();
	}

	public void run() {

		makeFilePathAndUrlMap();

		// PRINT START
		UIMesseger.printStartProcess(filePathAndUrlMap.size());

		sendThreads(null);
	}

	private void sendThreads(ArrayList<FutureTask<DownloadThread>> futureTasks) {
		// TODO Auto-generated method stub
		this.threadPool = Executors.newFixedThreadPool(CONCURRENT_THREADS);

		if (futureTasks == null) {
			futureTasks = new ArrayList<FutureTask<DownloadThread>>();

			List<Map.Entry<String, String>> urlAndFilesList = new ArrayList<Map.Entry<String, String>>(
					filePathAndUrlMap.entrySet());

			// each time you want a different order.
			Collections.shuffle(urlAndFilesList);
			for (Map.Entry<String, String> entry : urlAndFilesList) {

				String url = entry.getValue();
				String filePath = entry.getKey();
				DownloadThread dt = new DownloadThread(url, filePath);
				FutureTask<DownloadThread> ft = new FutureTask<DownloadThread>(
						dt);
				futureTasks.add(ft);
				threadPool.execute(ft);
			}
		} else {
			ArrayList<FutureTask<DownloadThread>> newFutureTasks = new ArrayList<FutureTask<DownloadThread>>();
			for (int i = 0; i < futureTasks.size(); i++) {
				FutureTask<DownloadThread> ft = futureTasks.get(i);
				DownloadThread dt = null;
				try {
					dt = ft.get();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				ft = new FutureTask<DownloadThread>(dt);
				newFutureTasks.add(ft);
			}

			futureTasks = newFutureTasks;

			for (int i = 0; i < futureTasks.size(); i++) {
				FutureTask<DownloadThread> ft = futureTasks.get(i);
				threadPool.execute(ft);
			}

		}

		checkThreads(futureTasks);

	}

	private void checkThreads(ArrayList<FutureTask<DownloadThread>> futureTasks) {
		boolean checkIsFinished = false;
		// Print processing
		UIMesseger.printProcessing(futureTasks.size());

		ArrayList<FutureTask<DownloadThread>> newFutureTasks = new ArrayList<FutureTask<DownloadThread>>();
		while (!checkIsFinished) {
			int totalCompleted = 0;
			boolean[] isDone = new boolean[futureTasks.size()];
			for (int i = 0; i < futureTasks.size(); i++) {
				FutureTask<DownloadThread> ft = futureTasks.get(i);
				isDone[i] = ft.isDone();
				if (!ft.isDone()) {
					isDone[i] = ft.isCancelled();
					if (isDone[i])
						System.out.println("CANCELLED");
				}
				if (isDone[i] == true)
					UIMesseger.addPercentage(totalCompleted++);
			}

			boolean completProcess = true;
			for (int i = 0; i < isDone.length; i++) {
				if (isDone[i] == false)
					completProcess = false;

			}

			if (completProcess) {
				for (int i = 0; i < futureTasks.size(); i++) {
					FutureTask<DownloadThread> ft = futureTasks.get(i);
					try {
						DownloadThread dT = ft.get(10, TimeUnit.MILLISECONDS);
						// Show only right
						if (dT.isFinishedResult() == 701
								|| dT.isFinishedResult() == 702) {
							// Nothing to do
						} else {
							newFutureTasks.add(ft);
						}
						UIMesseger.addCode(dT.isFinishedResult());
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ExecutionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				futureTasks = newFutureTasks; // swap
				this.threadPool.shutdown();
				UIMesseger.printResults(futureTasks.size());
				checkIsFinished = true;

				// recursivity;
				if (newFutureTasks.size() > 0) {
					this.sendThreads(newFutureTasks);
				}
				return;
			}
		}

	}

	private void makeFilePathAndUrlMap() {
		String fileUrl;
		String filePath;

		int currentNumber = 1;
		for (int i = 0; i < minDepth; i++) {
			currentNumber = currentNumber * 2;
		}

		if (currentNumber == 0)
			currentNumber = 1;

		for (int i = minDepth; i < maxDepth + 1; i++) {

			for (int j = 0; j < currentNumber; j++) {
				for (int k = 0; k < currentNumber; k++) {
					fileUrl = String.format(FORMATTED_DESTINATION_URL, i, j, k);
					filePath = ROOT_PATH + i + File.separator + j
							+ File.separator + k + ".png";
					filePathAndUrlMap.put(filePath, fileUrl);
				}
			}
			currentNumber *= 2;
		}
	}

}
