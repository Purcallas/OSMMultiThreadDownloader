SM Multi Thread Downloader 
===========================

This software allows to download directly the tile maps from Open Street Maps.
Makes a map folder with all the tiles downloaded.

The user must specify by parammeter the min depth and max depth of the map tilex matrix. 
And also can set the number of concurrent threads.


Software under GNU 3.0 license


USE
===
Used by console

java -jar osmultithreaddownloader.jar {required: [minDepth number] [maxDepth number]} {optional: -t [concurrentThreads number]  

Example:

java -jar osmmultithreaddownloader.jar 2 3

java -jar osmmultithreaddownloader.jar 4 4 -t 50

